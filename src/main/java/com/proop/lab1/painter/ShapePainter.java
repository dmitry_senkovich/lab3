package com.proop.lab1.painter;

import com.proop.lab1.shape.Shape;

public interface ShapePainter<S extends Shape> {

    String paint(Integer[] coordinates);

}
