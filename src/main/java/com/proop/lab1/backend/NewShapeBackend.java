package com.proop.lab1.backend;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;
import com.proop.lab1.helper.ShapeContainer;
import com.proop.lab1.helper.ShapeRegistry;
import com.proop.lab1.shape.Shape;
import com.proop.lab1.shape.factory.ShapeFactory;
import com.proop.lab1.shape.factory.abstractfactory.ShapeFactoryAbstractFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class NewShapeBackend {

    private ShapeRegistry shapeRegistry = ShapeRegistry.getInstance();
    private ShapeFactoryAbstractFactory shapeFactoryAbstractFactory = ShapeFactoryAbstractFactory.getInstance();
    private ShapeContainer shapeContainer = new ShapeContainer();

    private String currentShapeName = "";
    private List<Point> currentShapePoints = new LinkedList<>();
    private List<Line> currentShapeLines = new LinkedList<>();

    public NewShapeBackend() {}

    public NewShapeBackend(ShapeContainer shapeContainer) {
        this.shapeContainer = shapeContainer;
    }

    public void setShapeContainer(ShapeContainer shapeContainer) {
        this.shapeContainer = shapeContainer;
    }

    public void resetCurrentShape() {
        currentShapeName = "";
        currentShapePoints = new LinkedList<>();
        currentShapeLines = new LinkedList<>();
    }

    public List<String> getShapeNames() {
        return shapeRegistry.getRegisteredShapeNames();
    }

    public void setCurrentShapeName(String shapeName) {
        currentShapeName = shapeName;
    }

    public void addPoint(Point point) {
        currentShapePoints.add(point);
    }

    public void addLine(Line line) {
        currentShapeLines.add(line);
    }

    public String getCurrentPoints() {
        return currentShapePoints.stream().map(Point::toString).collect(Collectors.joining(", "));
    }

    public String getCurrentLines() {
        return currentShapeLines.stream().map(Line::toString).collect(Collectors.joining(", "));
    }

    public boolean createShape() {
        boolean created = false;
        try {
            Class<? extends Shape> currentShapeClass = shapeRegistry.getRegisteredShapes().get(currentShapeName);
            ShapeFactory shapeFactory = shapeFactoryAbstractFactory.getFactoryObject(currentShapeClass);
            Shape shape = shapeFactory.createShape(currentShapePoints, currentShapeLines);
            shapeContainer.add(shape);
            created = true;
            resetCurrentShape();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Failed to create a new shape");
        }

        return created;
    }

}
