package com.proop.lab1.abstractfactory;

import com.proop.lab1.shape.Shape;

public interface AbstractFactory<F> {

    void registerShape(Class<? extends Shape> shapeClass, F factoryObject);

    F getFactoryObject(Class<? extends Shape> shapeClazz);

}
