/*
 * Created by JFormDesigner on Mon Mar 12 14:18:34 MSK 2018
 */

package com.proop.lab1.frontend;

import com.proop.lab1.backend.EditShapeListBackend;
import com.proop.lab1.backend.NewShapeBackend;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * @author Dmitry Senkovich
 */
public class EditShapeListForm extends JPanel {

    private EditShapeListBackend editShapeListBackend;

    public EditShapeListForm(File file) throws IOException {
        initComponents();
        editShapeListBackend = new EditShapeListBackend(file);
        initForm();
    }

    private void initForm() {
        resetShapeListComboBox();
        resetCurrentShape();
    }

    private void resetCurrentShape() {
        setCurrentShape(0);
    }

    private void setCurrentShape(int index) {
        editShapeListBackend.setCurrentShapeIndex(index);
        shapeComboBox.setSelectedIndex(index);
    }

    private void shapeComboBoxItemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            setCurrentShape(shapeComboBox.getSelectedIndex());
        }
    }

    private void editShapeButtonMousePressed(MouseEvent e) {
        final JDialog frame = new JDialog((JFrame) SwingUtilities.getWindowAncestor(this), "Edit shape", true);
        frame.getContentPane().add(new EditShapeForm(editShapeListBackend.getCurrentShape()));
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                editShapeListBackend.updateShapesPainted();
                resetShapeListComboBox();
                setCurrentShape(editShapeListBackend.getCurrentShapeIndex());
            }
        });
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private void deleteShapeButtonMousePressed(MouseEvent e) {
        if (editShapeListBackend.getShapesPainted().isEmpty()) {
            return;
        }

        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(null,
                "Are you sure you want to remove selected shape?", "Confirming shape removal", dialogButton);
        if(dialogResult == JOptionPane.YES_OPTION){
            editShapeListBackend.removeCurrentShape();
            shapeComboBox.removeItemAt(shapeComboBox.getSelectedIndex());
            if (!editShapeListBackend.getShapesPainted().isEmpty()) {
                resetCurrentShape();
            }
            JOptionPane.showMessageDialog(this, "Shape is deleted!");
        }
    }

    private void addShapeButtonMousePressed(MouseEvent e) {
        final JDialog frame = new JDialog((JFrame) SwingUtilities.getWindowAncestor(this), "Add new shape", true);
        frame.getContentPane().add(new NewShapeForm(new NewShapeBackend(editShapeListBackend.getShapeContainer())));
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                resetShapeListComboBox();
                resetCurrentShape();
            }
        });
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private void resetShapeListComboBox() {
        editShapeListBackend.updateShapesPainted();
        shapeComboBox.setModel(new DefaultComboBoxModel(editShapeListBackend.getShapesPainted().toArray()));
    }

    private void saveChangesButtonMousePressed(MouseEvent e) {
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(null,
                "Are you sure you want to save shape list?", "Confirming shape list saving", dialogButton);
        if(dialogResult == JOptionPane.YES_OPTION){
            try {
                editShapeListBackend.saveShapeList();
                JOptionPane.showMessageDialog(this, "Shapes are saved!");
            } catch (IOException e1) {
                System.out.println("Failed to save shapes");
                JOptionPane.showMessageDialog(this,
                        "Failed to save shapes",
                        "Error while saving shapes",  JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Dmitry Senkovich
        shapeComboBox = new JComboBox();
        editShapeButton = new JButton();
        deleteShapeButton = new JButton();
        saveChangesButton = new JButton();
        addShapeButton = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(320, 285));
        setMaximumSize(new Dimension(320, 285));
        setPreferredSize(new Dimension(320, 285));

        // JFormDesigner evaluation mark
        setBorder(new javax.swing.border.CompoundBorder(
            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

        setLayout(null);

        //---- shapeComboBox ----
        shapeComboBox.addItemListener(e -> shapeComboBoxItemStateChanged(e));
        add(shapeComboBox);
        shapeComboBox.setBounds(15, 15, 280, shapeComboBox.getPreferredSize().height);

        //---- editShapeButton ----
        editShapeButton.setText("Edit shape");
        editShapeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                editShapeButtonMousePressed(e);
            }
        });
        add(editShapeButton);
        editShapeButton.setBounds(new Rectangle(new Point(10, 60), editShapeButton.getPreferredSize()));

        //---- deleteShapeButton ----
        deleteShapeButton.setText("Delete Shape");
        deleteShapeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                deleteShapeButtonMousePressed(e);
            }
        });
        add(deleteShapeButton);
        deleteShapeButton.setBounds(new Rectangle(new Point(105, 60), deleteShapeButton.getPreferredSize()));

        //---- saveChangesButton ----
        saveChangesButton.setText("Save changes");
        saveChangesButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                saveChangesButtonMousePressed(e);
            }
        });
        add(saveChangesButton);
        saveChangesButton.setBounds(new Rectangle(new Point(100, 240), saveChangesButton.getPreferredSize()));

        //---- addShapeButton ----
        addShapeButton.setText("Add shape");
        addShapeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                addShapeButtonMousePressed(e);
            }
        });
        add(addShapeButton);
        addShapeButton.setBounds(new Rectangle(new Point(215, 60), addShapeButton.getPreferredSize()));

        setPreferredSize(new Dimension(320, 285));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Dmitry Senkovich
    private JComboBox shapeComboBox;
    private JButton editShapeButton;
    private JButton deleteShapeButton;
    private JButton saveChangesButton;
    private JButton addShapeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
