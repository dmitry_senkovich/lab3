package com.proop.lab1.frontend;

import com.proop.lab1.initializer.ShapeFactoryAbstractFactoryInitializer;
import com.proop.lab1.initializer.ShapePainterFactoryInitializer;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;

import static javax.swing.JFileChooser.APPROVE_OPTION;

public class AppFrame extends JFrame {

    public AppFrame() {
        ShapeFactoryAbstractFactoryInitializer.getInstance().init();
        ShapePainterFactoryInitializer.getInstance().init();

        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem newMenuItem = new JMenuItem("New");
        final JFrame thisJFrame = this;
        newMenuItem.addActionListener(e -> {
            thisJFrame.getContentPane().removeAll();
            thisJFrame.add(new NewShapeListForm());
            thisJFrame.revalidate();
        });
        JMenuItem loadMenuItem = new JMenuItem("Load");
        loadMenuItem.addActionListener(e -> {
            JFileChooser jFileChooser = new JFileChooser();
            int returnValue = jFileChooser.showOpenDialog(this);
            if (APPROVE_OPTION == returnValue) {
                thisJFrame.getContentPane().removeAll();
                File file = jFileChooser.getSelectedFile();
                try {
                    thisJFrame.add(new EditShapeListForm(file));
                } catch (IOException exception) {
                    System.out.println("Failed to load shapes from file: " + file.getName());
                    thisJFrame.add(new NewShapeListForm());
                }
                thisJFrame.revalidate();
                thisJFrame.pack();
            }
        });
        fileMenu.add(newMenuItem);
        fileMenu.add(loadMenuItem);
        menuBar.add(fileMenu);

        this.setTitle("Lab 2");
        this.add(new NewShapeListForm());
        this.setJMenuBar(menuBar);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

}
