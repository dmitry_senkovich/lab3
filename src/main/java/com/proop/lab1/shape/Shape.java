package com.proop.lab1.shape;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;

import java.util.List;

public interface Shape {

    Integer[] getCoordinates();

    String getShapeName();

    List<Point> getPoints();
    void setPoints(List<Point> points);
    List<Line> getLines();
    void setLines(List<Line> lines);

}
