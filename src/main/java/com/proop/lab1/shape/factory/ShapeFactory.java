package com.proop.lab1.shape.factory;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;
import com.proop.lab1.shape.Shape;

import java.util.List;

public interface ShapeFactory<S extends Shape> {

    S createShape(List<Point> points, List<Line> lines);

}
