package com.proop.lab1.shape.factory;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;
import com.proop.lab1.shape.Triangle;

import java.util.List;

public class TriangleFactory implements ShapeFactory<Triangle> {

    @Override
    public Triangle createShape(List<Point> points, List<Line> lines) {
        return new Triangle(points.get(0), points.get(1), points.get(2));
    }

}
