package com.proop.lab1.shape.factory;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;
import com.proop.lab1.shape.Circle;

import java.util.List;

public class CircleFactory implements ShapeFactory<Circle> {

    @Override
    public Circle createShape(List<Point> points, List<Line> lines) {
        return new Circle(points.get(0), lines.get(0));
    }

}
