package com.proop.lab1.shape.factory;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;
import com.proop.lab1.shape.Square;

import java.util.List;

public class SquareFactory implements ShapeFactory<Square> {

    @Override
    public Square createShape(List<Point> points, List<Line> lines) {
        return new Square(points.get(0), lines.get(0));
    }

}
