package com.proop.lab1.shape.factory;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;
import com.proop.lab1.shape.Rectangle;

import java.util.List;

public class RectangleFactory implements ShapeFactory<Rectangle> {

    @Override
    public Rectangle createShape(List<Point> points, List<Line> lines) {
        return new Rectangle(points.get(0), lines.get(0), lines.get(1));
    }

}
