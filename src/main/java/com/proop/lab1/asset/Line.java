package com.proop.lab1.asset;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Line {

    Integer length;

    @JsonCreator
    public Line(@JsonProperty("length") Integer length) {
        this.length = length;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return String.format("[%d]", length);
    }

}
